Feature: Spelling a single word

  Scenario Outline: Check that incorrect word would be spelled to it correct form (get request)
    Given Incorrect word <incorrectWord>
    When I send request with incorrect word to <serviceType> service by get
    Then I will receive response with corrected word <correctedWord>

    Examples:
    |   incorrectWord   |   correctedWord   |   serviceType   |
    | "синхрафазатрон"  | "синхрофазотрон"  |     "XML"       |
    |   "болалайка"     |    "балалайка"    |     "JSON"      |
    |     "малкула"     |     "молекула"    |     "XML"       |
    |     "малако"      |      "молоко"     |     "JSON"      |
    |   "sametimes"     |    "sometimes"    |     "XML"       |
    |   "beoutiful"     |    "beautiful"    |     "JSON"      |

  Scenario Outline: Check that incorrect word would be spelled to it correct form (get request)
    Given Incorrect word <incorrectWord>
    When I send request with incorrect word to <serviceType> service by post
    Then I will receive response with corrected word <correctedWord>

    Examples:
      |   incorrectWord   |   correctedWord   |   serviceType   |
      | "синхрафазатрон"  | "синхрофазотрон"  |     "XML"       |
      |   "болалайка"     |    "балалайка"    |     "JSON"      |
      |     "малкула"     |     "молекула"    |     "XML"       |
      |     "малако"      |      "молоко"     |     "JSON"      |
      |   "sametimes"     |    "sometimes"    |     "XML"       |
      |   "beoutiful"     |    "beautiful"    |     "JSON"      |




