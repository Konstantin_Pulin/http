package com.datalex.tests;

import com.datalex.ServiceURIs;
import com.datalex.httpclient.HttpRequestsHelper;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.amarcinkowski.fastxpathaccess.Xpath;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.junit.Assert;

import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Konstantin_Pulin on 11/3/2016.
 * NB!!! Steps in this class asserts expected results as strings
 */
public class CheckWordSteps {

    private String incorrectWord;
    private HttpRequestsHelper httpRequestsHelper;
    private String correctedWord;
    public static final Logger LOG = Logger.getLogger(CheckWordSteps.class);

    @Given("^Incorrect word \"([^\"]*)\"$")
    public void incorrectWordReceived(String incorrectWord) {
        this.incorrectWord = incorrectWord;
        LOG.info("Given incorrect word: " + incorrectWord);
    }


    @When("^I send request with incorrect word to \"([^\"]*)\" service by get$")
    public void sendGetRequestWithIncorrectWord(String format) throws Throwable {
        httpRequestsHelper = new HttpRequestsHelper();
        Map<String, String> requestParameters = new HashMap<String, String>() {{
            put("text",incorrectWord);
        }};
        getResponseInFormat(format, "checkText", requestParameters, "get");
    }

    @When("^I send request with incorrect word to \"([^\"]*)\" service by post$")
    public void sendPostRequestWithIncorrectWord(String format) throws Throwable {
        httpRequestsHelper = new HttpRequestsHelper();
        Map<String, String> requestParameters = new HashMap<String, String>() {{
            put("text",incorrectWord);
        }};
        getResponseInFormat(format, "checkText", requestParameters, "post");
    }

    @Then("^I will receive response with corrected word \"([^\"]*)\"$")
    public void receiveResponseWithCorrectedWord(String expected) throws Throwable {
        LOG.info("Checking that " + expected + " equals to " + correctedWord);
        Assert.assertEquals(expected, correctedWord);
    }

    private void getResponseInFormat(String format, String requestFunction, Map<String, String> requestParameters, String requestMethod) throws XPathExpressionException, IOException {
        requestMethod = requestMethod.equalsIgnoreCase("get")||requestMethod.equalsIgnoreCase("post") ? requestMethod : "get";
        String response = "";
        if (format.equals("XML")) {
            if (requestMethod.equalsIgnoreCase("get")) {
                response = httpRequestsHelper.sendGetRequest(ServiceURIs.YANDEX_XML_SPELLER, requestFunction, requestParameters);
            } else if (requestMethod.equalsIgnoreCase("post")) {
                response = httpRequestsHelper.sendPostRequest(ServiceURIs.YANDEX_XML_SPELLER,
                        requestFunction, requestParameters);
            }
            LOG.info("Received response in " + format + " format " + response);
            correctedWord = Xpath.find(response, "/SpellResult/error[1]/s")[0];
        } else if (format.equals("JSON")) {
            if (requestMethod.equalsIgnoreCase("get")) {
                response = httpRequestsHelper.sendGetRequest(ServiceURIs.YANDEX_JSON_SPELLER, requestFunction, requestParameters);
            } else if (requestMethod.equalsIgnoreCase("post")) {
                response = httpRequestsHelper.sendPostRequest(ServiceURIs.YANDEX_JSON_SPELLER,
                        requestFunction, requestParameters);
            }
            JSONArray responses = new JSONArray(response);
            correctedWord =  responses.getJSONObject(0).getJSONArray("s").getString(0);
        }
        LOG.info("Received corrected word from response: " + correctedWord);
    }
}
