package com.datalex.tests;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.log4j.Logger;


/**
 * Created by Konstantin_Pulin on 11/3/2016.
 */
public class CucumberBase {

    public static final Logger LOG = Logger.getLogger(CucumberBase.class);

    @Before(order = 1)
    public void setUp() {
        LOG.info("SETUP");
    }

    @After(order = 10000)
    public void tearDown() {
        LOG.info("TEAR DOWN");
    }


}
