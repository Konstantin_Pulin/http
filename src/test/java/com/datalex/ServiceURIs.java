package com.datalex;

/**
 * Created by Konstantin_Pulin on 11/3/2016.
 */
public class ServiceURIs {
    public static final String YANDEX_XML_SPELLER = "http://speller.yandex.net/services/spellservice/";
    public static final String YANDEX_JSON_SPELLER = "http://speller.yandex.net/services/spellservice.json/";
}
