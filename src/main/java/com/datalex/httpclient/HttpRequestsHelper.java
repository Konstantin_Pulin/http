package com.datalex.httpclient;

import okhttp3.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Konstantin_Pulin on 11/3/2016.
 */
public class HttpRequestsHelper {

    public String sendGetRequest(String url, String method, Map<String, String> parameters) throws IOException {

        StringBuilder sb = new StringBuilder().append(url).append(method).append("?");
        for (Map.Entry<String, String> entry: parameters.entrySet()){
            sb.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(sb.toString())
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();

    }

    public String sendPostRequest(String url, String method, Map<String, String> parameters) throws IOException {
        StringBuilder sb = new StringBuilder().append(url).append(method);
        OkHttpClient client = new OkHttpClient();

        FormBody.Builder formBodyBuilder = new FormBody.Builder();
        for (Map.Entry<String, String> entry: parameters.entrySet()){
            formBodyBuilder.add(entry.getKey(), entry.getValue());
        }

        RequestBody formBody = formBodyBuilder.build();

        Request request = new Request.Builder()
                .url(sb.toString())
                .post(formBody)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }
}
